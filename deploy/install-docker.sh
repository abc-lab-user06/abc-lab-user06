#!/bin/sh

set -eu

# Docker
sudo apt remove --yes docker docker-engine docker.io \
    && sudo apt update \
    && sudo apt --yes --no-install-recommends install \
        apt-transport-https \
        ca-certificates \
    && wget --quiet --output-document=- https://download.docker.com/linux/ubuntu/gpg \
        | sudo apt-key add - \
    && sudo add-apt-repository \
        "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu \
        $(lsb_release --codename --short) \
        stable" \
    && sudo apt update \
    && sudo apt --yes --no-install-recommends install docker-ce \
    && sudo usermod --append --groups docker "admin-user" \
    && sudo systemctl enable docker \
    && printf '\nDocker installed successfully\n\n'
printf 'Waiting for Docker to start...\n\n'
sleep 3

# Docker Compose
sudo wget \
        --output-document=/usr/local/bin/docker-compose \
        https://github.com/docker/compose/releases/download/1.24.0/run.sh \
    && sudo chmod +x /usr/local/bin/docker-compose \
    && sudo wget \
        --output-document=/etc/bash_completion.d/docker-compose \
        "https://raw.githubusercontent.com/docker/compose/1.24.0/contrib/completion/bash/docker-compose" \
    && printf '\nDocker Compose installed successfully\n\n'

sudo cat >docker-compose.yml <<EOL
version: '3.1'

services:

  elasticsearch:
   image: docker.elastic.co/elasticsearch/elasticsearch:7.0.1
   container_name: elasticsearch
   ports:
    - "9200:9200"
   environment:
    - discovery.type=single-node
   volumes:
    - elasticsearch-data:/usr/share/elasticsearch/data
   networks:
    - docker-network

  kibana:
   image: docker.elastic.co/kibana/kibana:7.0.1
   container_name: kibana
   ports:
    - "5601:5601"
   depends_on:
    - elasticsearch
   networks:
    - docker-network

networks:
  docker-network:
    driver: bridge

volumes:
  elasticsearch-data:
EOL
sleep 3
docker stop $(docker ps -a -q) &>/dev/null
sleep 3
docker rm $(docker ps -a -q) &>/dev/null
sleep 3
sudo docker-compose -f docker-compose.yml up -d
